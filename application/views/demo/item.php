<h1>REST API<h1>
<h2>Requirements</h2>
<p><strong>Task 1: </strong> You are to create a controller and AJAX function to make REST API call to a URL to perform the following task:</p>
<ol>
    <li>Create a controller called Main.php</li>
    <li>Within this page, create a simple form with two parameters
        <ul>
            <li>firstName</li>
            <li>secondName</li>
        </ul>
    </li>
    <li>Create a button with the form above. On click, the button should call a AJAX function (for example, <code>function getSuggestedWebId(firstName, secondName)</code> ) within this page</li>
    <li>The value of the firstName and lastName should be grabbed from the <code>< input id="firstName" type="text" name="firstName" value="" /></code> and <code>< input id="lastName" type="text" name="lastName" value="" /></code></li>
    <li>Write a cURL function to call the following URL: <code>http://im.gdbg.com:8080/commsapplib/ws/clSuggestWebId</code> with the required parameters</li>
    <li>The AJAX function within this page will call the controller function (for eg, http://localhost/coding_challenge/main/getSuggestedWebId) and if successful, print out the suggested webID name</li>
    <li>Embed all required codes and scripts within this page</li>
</ol>

<!--Start Coding-->



<!--End Coding-->